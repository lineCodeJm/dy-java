package com.dyj.applet.handler;

import com.dyj.applet.domain.query.LimitOpPointQuery;
import com.dyj.applet.domain.query.OrderCizStatusQuery;
import com.dyj.applet.domain.query.RegisterMaAppQuery;
import com.dyj.applet.domain.query.ShopMemberLeaveQuery;
import com.dyj.applet.domain.vo.OrderCizStatusVo;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DySimpleResult;
import com.dyj.common.domain.vo.BaseVo;

/**
 * @author danmo
 * @date 2024-04-28 11:00
 **/
public class AptEAppletHandler extends AbstractAppletHandler {
    public AptEAppletHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 注册小程序积分阈值
     *
     * @param query 入参
     * @return DySimpleResult
     */
    public DySimpleResult<BaseVo> limitOpPoint(LimitOpPointQuery query) {
        baseQuery(query);
        return getEAppletClient().limitOpPoint(query);
    }

    /**
     * 注册小程序预览图、定制类小程序开发者注册信息
     *
     * @param query 入参
     * @return DySimpleResult<BaseVo>
     */
    public DySimpleResult<BaseVo> registerMaApp(RegisterMaAppQuery query) {
        baseQuery(query);
        return getEAppletClient().registerMaApp(query);
    }

    /**
     * 查询订单的定制完成状态
     * @param appId 小程序ID
     * @param openId 用户ID
     * @param orderId 订单ID
     * @return DySimpleResult<OrderCizStatusVo>
     */
    public DySimpleResult<OrderCizStatusVo> queryOrderCustomizationStatus(String appId, String openId, String orderId) {
        return getEAppletClient().queryOrderCustomizationStatus(OrderCizStatusQuery.builder()
                .appId(appId)
                .openId(openId)
                .orderId(orderId)
                .tenantId(agentConfiguration.getTenantId())
                .clientKey(agentConfiguration.getClientKey())
                .build());
    }

    /**
     * 退会
     * 店铺会员退出
     * @param appId 小程序ID
     * @param openId 用户ID
     * @param shopId 会员ID
     * @return DySimpleResult<BaseVo>
     */
    public DySimpleResult<BaseVo> shopMemberLeave(String appId, String openId, Long shopId) {
        return getEAppletClient().shopMemberLeave(ShopMemberLeaveQuery.builder()
                .appId(appId)
                .openId(openId)
                .shopId(shopId)
                .tenantId(agentConfiguration.getTenantId())
                .clientKey(agentConfiguration.getClientKey())
                .build());
    }
}
