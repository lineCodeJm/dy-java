package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-04-28 14:24
 **/
public class ScenicNoticeIntro {

    /**
     * 节点内容
     */
    private String content;

    /**
     * 富文本处理中的节点类型
     * 1 - text
     * 2 - image
     */
    private String node_type;

    public static SupplierScenicNoticePlayIntroBuilder builder() {
        return new SupplierScenicNoticePlayIntroBuilder();
    }

    public static class SupplierScenicNoticePlayIntroBuilder {
        private String content;
        private String nodeType;

        public SupplierScenicNoticePlayIntroBuilder content(String content) {
            this.content = content;
            return this;
        }

        public SupplierScenicNoticePlayIntroBuilder nodeType(String nodeType) {
            this.nodeType = nodeType;
            return this;
        }

        public ScenicNoticeIntro build() {
            ScenicNoticeIntro scenicNoticeIntro = new ScenicNoticeIntro();
            scenicNoticeIntro.setContent(content);
            scenicNoticeIntro.setNode_type(nodeType);
            return scenicNoticeIntro;
        }
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNode_type() {
        return node_type;
    }

    public void setNode_type(String node_type) {
        this.node_type = node_type;
    }
}
