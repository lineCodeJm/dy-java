package com.dyj.applet.domain.vo;

import java.util.List;

public class ConsumeCouponIdListVo {

    private List<ConsumeCouponIdResult> results;

    public List<ConsumeCouponIdResult> getResults() {
        return results;
    }

    public ConsumeCouponIdListVo setResults(List<ConsumeCouponIdResult> results) {
        this.results = results;
        return this;
    }
}
